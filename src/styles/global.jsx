import { createGlobalStyle } from "styled-components";

const GlobalStyle = createGlobalStyle`
    * {
        margin: 0;
        padding: 0;
        box-sizing: border-box;
        outline: 0;
    }

    :root {
        --darkblue: #08043c;
        --lightblue: #1480fb;
        --white: #ffffff;
        --purple: #6d0c91;
        --gray: #212529;
        --lightgreen: #00ffff;
        --red: #DC4731;
    }

    body, input {
        font-family: 'Open Sans', sans-serif;
    }

    button {
        font-family: 'Ubuntu', sans-serif;
        cursor: pointer;
    }

    h1, h2, h3 {
        font-family: 'Otomanopee One', sans-serif;
    }
`;

export default GlobalStyle;
