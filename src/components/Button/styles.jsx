import styled from "styled-components";

export const Button = styled.button`
  background: ${(props) => (props.purpleSchema ? "#6d0c91" : "#1480fb")};
  color: var(--white);
  border: 0;
  border-radius: 10px;
  height: 45px;
  font-size: 1.5rem;
  width: 100%;
  :hover {
    background: ${(props) => (props.purpleSchema ? "#1480fb" : "#6d0c91")};
  }
`;
