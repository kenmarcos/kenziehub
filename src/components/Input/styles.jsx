import styled from "styled-components";

export const Input = styled.input`
  width: 100%;
  border-radius: 5px;
  height: 35px;
  font-size: 1rem;
  margin-bottom: 10px;
  padding: 0 10px;
`;
