import { Container } from "./styles";

const Figure = ({ url }) => {
  return <Container url={url}></Container>;
};

export default Figure;
