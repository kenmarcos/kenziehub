import { Container } from "./styles";
import ComponentButton from "../Button";

const Card = ({ tech, deleteTech }) => {
  return (
    <Container aria-label="tech card">
      <div>
        <h3>Tecnologia: {tech.title}</h3>
        <p>Nível: {tech.status}</p>
      </div>
      <div>
        <ComponentButton purpleSchema onClick={() => deleteTech(tech.id)}>
          Remover
        </ComponentButton>
      </div>
    </Container>
  );
};

export default Card;
