import styled from "styled-components";

export const Container = styled.div`
  border-top: 1px solid var(--gray);
  border-bottom: 1px solid var(--gray);
  padding: 15px;
  margin: 5px 0;
  display: flex;
  flex-direction: column;
  align-items: center;

  background-color: var(--lightgreen);
  h3,
  p {
    color: var(--darkblue);
  }

  div {
    width: 100%;
  }
  div:last-child {
    width: 40%;
  }

  button {
    margin: 10px 0;
    font-size: 1.3rem;
  }

  @media screen and (min-width: 1024px) {
    flex-direction: row;
    justify-content: space-between;
    div:last-child {
      width: 30%;
    }
    button {
      padding: 10px;
      margin: 0;
      width: 100%;
    }
  }
`;
