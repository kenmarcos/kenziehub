import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import ComponentInput from "../Input";
import ComponentButton from "../Button";
import { Form } from "./styles";

const TechsForm = ({ addTech }) => {
  const schema = yup.object().shape({
    title: yup.string().required("Campo obrigatório"),
    status: yup.string().required("Campo obrigatório"),
  });

  const {
    register,
    handleSubmit,
    formState: { errors },
    reset,
  } = useForm({ resolver: yupResolver(schema) });

  const onSubmitFunction = (data) => {
    addTech(data);
    reset();
  };

  return (
    <Form onSubmit={handleSubmit(onSubmitFunction)}>
      <div>
        <span>{errors.title?.message}</span>
        <ComponentInput
          placeholder="Tecnologia"
          register={register}
          inputName="title"
        />
        <span>{errors.status?.message}</span>
        <ComponentInput
          placeholder="Nível de conhecimento"
          register={register}
          inputName="status"
        />
      </div>
      <div>
        <ComponentButton type="submit" purpleSchema>
          Adicionar
        </ComponentButton>
      </div>
    </Form>
  );
};

export default TechsForm;
