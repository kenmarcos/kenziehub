import React from "react";
import ComponentInput from "../Input";
import ComponentButton from "../Button";
import { Form } from "./styles";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import api from "../../services/api";
import { useHistory } from "react-router-dom";
import { toast } from "react-toastify";

const LoginForm = ({ setAuthenticated }) => {
  const history = useHistory();

  const schema = yup.object().shape({
    email: yup.string().email("Email inválido").required("Campo obrigatório"),
    password: yup
      .string()
      .min(6, "Mínimo de 6 dígitos")
      .required("Campo obrigatório"),
  });

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({ resolver: yupResolver(schema) });

  const onSubmitFunction = (data) => {
    api
      .post("/sessions", data)
      .then((response) => {
        toast.success("Tudo certo! Você entrou na sua conta.");
        localStorage.clear();
        localStorage.setItem(
          "@kenziehub:token",
          JSON.stringify(response.data.token)
        );
        setAuthenticated(true);
        return history.push("/dashboard");
      })
      .catch((_) => toast.error("E-mail ou senha inválidos."));
  };

  return (
    <Form onSubmit={handleSubmit(onSubmitFunction)}>
      <span>{errors.email?.message}</span>
      <ComponentInput
        placeholder="E-mail"
        register={register}
        inputName="email"
      />
      <span>{errors.password?.message}</span>
      <ComponentInput
        placeholder="Senha"
        register={register}
        inputName="password"
      />
      <ComponentButton type="submit">ENTRAR</ComponentButton>
    </Form>
  );
};

export default LoginForm;
