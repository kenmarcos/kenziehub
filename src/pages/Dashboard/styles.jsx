import styled from "styled-components";

export const Container = styled.div`
  height: 100vh;
  display: flex;
  flex-direction: column;
  align-items: center;
  h1 {
    color: var(--darkblue);
    padding: 15px;
    width: 100%;
  }
  h2 {
    color: var(--lightblue);
  }
`;

export const Content = styled.div`
  width: 90%;
  border: 2px solid var(--lightblue);
  border-radius: 10px;
  @media screen and (min-width: 1024px) {
    width: 60%;
  }
`;

export const Data = styled.div`
  border-bottom: 4px dotted var(--lightblue);
  padding: 15px;
  span {
    color: var(--gray);
  }
`;

export const FormContainer = styled.div`
  display: flex;
  justify-content: center;
  flex-wrap: wrap;
  padding: 15px;
  h2 {
    width: 100%;
    text-align: center;
    margin-bottom: 15px;
  }
`;

export const CardsContainer = styled.div`
  border-top: 4px dotted var(--lightblue);
`;

export const LogoutBtn = styled.button`
  background-color: var(--lightblue);
  color: var(--white);
  font-size: 1rem;
  padding: 5px 10px;
  position: absolute;
  right: 15px;
  top: 20px;
  border-radius: 5px;
  border: none;

  :hover {
    transform: scale(1.1);
  }

  svg {
    vertical-align: text-bottom;
    font-size: 1.2rem;
    margin-left: 5px;
  }
`;
