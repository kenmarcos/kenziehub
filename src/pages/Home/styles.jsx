import styled from "styled-components";

export const Container = styled.div`
  height: 100vh;
  display: flex;
  flex-direction: column;
  justify-content: space-evenly;

  @media screen and (min-width: 1024px) {
    flex-direction: row;
  }
`;

export const Content = styled.div`
  display: flex;
  justify-content: center;

  h1 {
    font-size: 3rem;
    margin: 15px 0;
    color: var(--lightblue);
  }
  span {
    font-size: 1.4rem;
    margin: 10px 0;
    color: var(--darkblue);
  }

  div {
    width: 80%;
  }
  button {
    margin: 15px 0;
  }
  @media screen and (min-width: 1024px) {
    width: 50%;
    flex-direction: column;
    justify-content: center;
    align-items: center;
  }
`;

export const ImageSection = styled.section`
  height: 50%;
  background-color: var(--lightblue);

  @media screen and (min-width: 1024px) {
    width: 50%;
    height: 100vh;
  }
`;
