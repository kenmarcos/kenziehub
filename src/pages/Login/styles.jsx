import styled from "styled-components";

export const Container = styled.div`
  height: 100vh;
  display: flex;
  flex-direction: column;
  justify-content: space-evenly;

  @media screen and (min-width: 1024px) {
    flex-direction: row;
  }
`;

export const Content = styled.div`
  display: flex;
  justify-content: center;

  h1 {
    font-size: 3rem;
    margin: 10px 0;
    color: var(--lightblue);
  }

  h2 {
    font-size: 1.8rem;
    color: var(--darkblue);
    margin-bottom: 10px;
  }

  div {
    width: 80%;
  }

  button {
    margin: 15px 0;
  }
  @media screen and (min-width: 1024px) {
    width: 50%;
    flex-direction: column;
    justify-content: center;
    align-items: center;
  }
`;

export const ImageSection = styled.section`
  display: none;
  @media screen and (min-width: 1024px) {
    display: block;
    background-color: var(--darkblue);
    width: 50%;
    height: 100vh;
  }
`;
